<?php

namespace prayingmantis\churchcaregroupslandord\Exceptions;

use Exception;

class TenantNullIdException extends Exception implements TenantExceptionInterface
{
    //
}
