<?php

namespace prayingmantis\churchcaregroupslandord\Exceptions;

use Exception;

class TenantNotSetException extends Exception implements TenantExceptionInterface
{
    //
}
