<?php

namespace prayingmantis\churchcaregroupslandord\Exceptions;

use Exception;

class TenantColumnUnknownException extends Exception implements TenantExceptionInterface
{
    //
}
